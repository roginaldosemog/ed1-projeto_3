## Projeto 3
Estrutura de Dados 1 - Turma B - Prof. Mateus Mendelson
Igor Aragão Gomes - 150011903
André Lucas Lemos - 150005563

### Rodando o programa
**Para compilar e executar o programa, execute os seguintes comandos na pasta raiz do projeto:**
- `$ gcc src/main.c -o bin/proj3`
- `$ ./bin/proj3`
