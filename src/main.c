#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct contact {
  char name[100];       // 100 caracteres
  char phone[10];       // XXXXX-XXXX
  char address[100];    // 100 caracteres
  int cep[8];           // 8 digitos
  char birth[10];       // dd/mm/aaa
} Contact;

void readFile();        //Abrir o arquivo //Ler o arquivo //Importar oq tem no arquivo contatos.txt, para os structs
void saveFile();        //Salva alterações no arquivo
void showMenu();        //Fazer o menuzinho
void insertContact();   //Implementa Inserir
void removeContact();   //Implementa Remover
void accessContact();   //Implementa Visualizar um unico contato
void orderContacts();   //Implementa ordenar lista de contatos
void listsContacts();   //Implementa Visualizar todos em ordem alfabetica (já tem que estar organizados)
void exitProgram();     //Sair

//Contacts ficam dentro de outro vetor ou de struct?

int main(int argc, char const *argv[]) {
  Contact *contact;

  readFile();

  return 0;
}

void readFile(){
  FILE *fp;

  if((fp = fopen("./doc/contatos.txt", "w+"))==NULL) {
    printf("Cannot open file.\n");
    exit(1);
  }

  printf("File opened.\n");

  fclose(fp);

  printf("File closed.\n");
}
